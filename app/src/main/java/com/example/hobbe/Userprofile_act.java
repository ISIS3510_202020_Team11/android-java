package com.example.hobbe;

import androidx.appcompat.app.AppCompatActivity;
import androidx.security.crypto.EncryptedSharedPreferences;
import androidx.security.crypto.MasterKeys;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.io.IOException;
import java.security.GeneralSecurityException;

public class Userprofile_act extends AppCompatActivity {

    TextView name;
    TextView phone;
    TextView email;
    TextView birthday;
    ImageView save;
    EditText bio;

    private FirebaseAuth mAuth;
    private DatabaseReference usersDB;
    SharedPreferences pre;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_userprofile_act);

        mAuth = FirebaseAuth.getInstance();
        final String currentUserId = mAuth.getCurrentUser().getUid();
        usersDB = FirebaseDatabase.getInstance().getReference().child("Users");


        name = findViewById(R.id.name);
        phone = findViewById(R.id.textView23);
        email = findViewById(R.id.textView24);
        birthday = findViewById(R.id.textView26);
        save = findViewById(R.id.firstsave);
        bio = findViewById(R.id.editTextBio);





        pre = getSharedPreferences(currentUserId, Context.MODE_PRIVATE);
        loadPreferences();


        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {






                SharedPreferences.Editor editor =  pre.edit();
                editor.putString("name", name.getText().toString());
                editor.putString("phone", phone.getText().toString());
                editor.putString("email", email.getText().toString());
                editor.putString("birthday", birthday.getText().toString());
                editor.putString("bio", bio.getText().toString());
                editor.commit();
                Toast.makeText(Userprofile_act.this, "Information updated!", Toast.LENGTH_SHORT).show();
                usersDB.child(currentUserId).child("name").setValue(name.getText().toString());
                usersDB.child(currentUserId).child("phone").setValue(phone.getText().toString());
                usersDB.child(currentUserId).child("quote").setValue(email.getText().toString());
                usersDB.child(currentUserId).child("birthday").setValue(birthday.getText().toString());
                usersDB.child(currentUserId).child("bio").setValue(bio.getText().toString());
            }
        });
    }

    public void loadPreferences(){
        final String currentUserId = mAuth.getCurrentUser().getUid();

        //SharedPreferences pre = getSharedPreferences(currentUserId, Context.MODE_PRIVATE);
        name.setText(pre.getString("name", "name"));
        phone.setText(pre.getString("phone", "phone"));
        email.setText(pre.getString("email", "quote"));
        birthday.setText(pre.getString("birthday", "birthday"));
        bio.setText(pre.getString("bio", "Biography"));
    }
}