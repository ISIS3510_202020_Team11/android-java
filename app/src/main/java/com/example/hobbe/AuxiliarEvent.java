package com.example.hobbe;

public class AuxiliarEvent {

    private String name;
    private String temperature = " " ;
    private Integer id ,image;

    private String  description, location, address, date,hour,category,cost,organizer,grupaln, lat, lon;


    public AuxiliarEvent(String name, String temperature, Integer id, String description, String location, String address, String date, String hour, int image, String category, String cost, String organizer, String grupaln, String lat, String lon) {
        this.name = name;
        this.temperature = temperature;
        this.id = id;
        this.description = description;
        this.location = location;
        this.address = address;
        this.date = date;
        this.hour = hour;
        this.image = image;
        this.category = category;
        this.cost = cost;
        this.organizer = organizer;
        this.grupaln = grupaln;
        this.lat = lat;
        this.lon = lon;
    }

    public String getName() {
        return name;
    }

    public String getTemperature() {
        return temperature;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setTemperature(String temperature) {
        this.temperature = temperature;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getImage() {
        return image;
    }

    public String getDescription() {
        return description;
    }

    public String getLocation() {
        return location;
    }

    public String getAddress() {
        return address;
    }

    public String getDate() {
        return date;
    }

    public String getHour() {
        return hour;
    }

    public String getCategory() {
        return category;
    }

    public String getCost() {
        return cost;
    }

    public String getOrganizer() {
        return organizer;
    }

    public String getGrupaln() {
        return grupaln;
    }

    public String getLat() {
        return lat;
    }

    public String getLon() {
        return lon;
    }
}
