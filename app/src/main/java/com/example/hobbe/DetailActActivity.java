package com.example.hobbe;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.fragment.app.Fragment;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

public class DetailActActivity extends AppCompatActivity {
    //
    ImageView img_principal;
    TextView text_name;
    TextView text_category;
    TextView text_date;
    TextView text_hour;
    TextView text_cost;
    TextView text_location;
    TextView text_address;

    ImageView icon_map;

    /// Maps stuff
    MapView map_view;
    private static final String MAPVIEW_BUNDLE_KEY = "MApBundleKey";



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.detail_act);

        text_name = findViewById(R.id.textView);
        img_principal = findViewById(R.id.imageView);
        text_category = findViewById(R.id.textView3);
        text_date= findViewById(R.id.textView6);
        text_hour= findViewById(R.id.textView4);
        text_cost= findViewById(R.id.textView7);
        text_location= findViewById(R.id.textView8);
        text_address= findViewById(R.id.textView9);
        int imag = getIntent().getIntExtra("image",0) == R.drawable.biking? R.drawable.biking: R.drawable.hiking;

        text_name.setText(getIntent().getStringExtra("name"));
        img_principal.setImageResource(imag);
        text_category.setText(getIntent().getStringExtra("category"));
        text_date.setText(getIntent().getStringExtra("date"));
        text_hour.setText(getIntent().getStringExtra("hour"));
        text_cost.setText(getIntent().getStringExtra("cost"));
        text_location.setText(getIntent().getStringExtra("location"));
        text_address.setText(getIntent().getStringExtra("address"));
        final String lat  = (getIntent().getStringExtra("lat"));
        final String lon =(getIntent().getStringExtra("long"));

        icon_map = findViewById(R.id.imageView3);
        icon_map.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (ConnectivityCheck() ) {

                    Intent intent = new Intent(DetailActActivity.this, MapsActivity.class);
                    intent.putExtra("lat", lat);
                    intent.putExtra("long", lon);
                    startActivity(intent);
                }
                else{
                    AlertMessage alert = new AlertMessage();
                    alert.show(getSupportFragmentManager(),"connection dialog");
                    //Toast.makeText(DetailActActivity.this, "There is not Internet connection.",Toast.LENGTH_SHORT).show();
                }

            }


        });





    }

    public  boolean ConnectivityCheck(){
        ConnectivityManager manager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = manager.getActiveNetworkInfo();
        if (networkInfo != null  && networkInfo.isConnected()){

            return true;
        }
        else{

            return false;
        }

    }





}