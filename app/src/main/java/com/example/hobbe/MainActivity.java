package com.example.hobbe;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.firebase.ui.auth.AuthUI;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class MainActivity extends AppCompatActivity {

   private EditText mEditTextName;
   private EditText mEditTextEmail;
   private EditText mEditTextPassword;
   private Button mButonRegister;
    private Button mButonLogIn;

   private String name = " ";
   private String email = " ";
   private String password = " ";



   FirebaseAuth mAuth;
   DatabaseReference mDatabase;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mAuth = FirebaseAuth.getInstance();
        mDatabase = FirebaseDatabase.getInstance().getReference();



        mEditTextName = (EditText)  findViewById(R.id.editTextName);
        mEditTextEmail = (EditText)  findViewById(R.id.editTextEmail);
        mEditTextPassword = (EditText)  findViewById(R.id.editTextPassword);
        mButonRegister = (Button)     findViewById(R.id.btnRegister);
        mButonLogIn = (Button) findViewById(R.id.btnDoLogIn);
        mButonLogIn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(MainActivity.this, Login.class));
            }
        });

        mButonRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                name =  mEditTextName.getText().toString();
                email =  mEditTextEmail.getText().toString();
                password =  mEditTextPassword.getText().toString();

                if (!name.isEmpty() && !email.isEmpty() && !password.isEmpty()){

                    registerUser();
                }
                else{
                    Toast.makeText(MainActivity.this,  "It is neccessary to  fill all the blanks", Toast.LENGTH_LONG).show();
                }
            }
        });

    }

    private void registerUser(){
        mAuth.createUserWithEmailAndPassword(email,password)
        .addOnCompleteListener(new OnCompleteListener<AuthResult>() {
            @Override
            public void onComplete(@NonNull Task<AuthResult> task) {
                if (task.isSuccessful()){

                    Map<String, Object>  map = new HashMap<>();
                    map.put( "name", name);
                    map.put( "email", email);
                    map.put( "password", password);

                    String id = mAuth.getCurrentUser().getUid();

                    mDatabase.push();
                    mDatabase.child("Users").child(id).setValue(map).addOnCompleteListener(new OnCompleteListener<Void>() {
                        @Override
                        public void onComplete(@NonNull Task<Void> task2) {

                            if (task2.isSuccessful()){
                                startActivity(new Intent(MainActivity.this, NewHome.class)); ////////////////////////

                                finish();/////////////////////////////
                            }
                            else{

                                Toast.makeText(MainActivity.this, "The data was not saved", Toast.LENGTH_SHORT).show();
                            }
                        }
                    });



                }
                else{

                    Toast.makeText(MainActivity.this, "The user could not be registered", Toast.LENGTH_SHORT).show();
                }
            }
        });



    }



}