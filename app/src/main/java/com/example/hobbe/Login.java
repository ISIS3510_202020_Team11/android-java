package com.example.hobbe;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;

public class Login extends AppCompatActivity {

    private EditText mEditTextEmail;
    private EditText mEditTextPassword;
    private Button mButonLogIn;

    private String email = " ";
    private String password = " ";

    private FirebaseAuth mAuth;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        mAuth = FirebaseAuth.getInstance();


        mEditTextEmail = (EditText)  findViewById(R.id.editTextEmail);
        mEditTextPassword = (EditText)  findViewById(R.id.editTextPassword);
        mButonLogIn = (Button)     findViewById(R.id.btnLogIn);

        mButonLogIn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                email =  mEditTextEmail.getText().toString();
                password =  mEditTextPassword.getText().toString();
                if(!email.isEmpty() &&  !password.isEmpty()){

                    loginUser();

                }
                else{

                    Toast.makeText(Login.this, " Fill all the spaces", Toast.LENGTH_SHORT).show();
                }

            }
        });
    }

    public void loginUser(){
        mAuth.signInWithEmailAndPassword(email, password).addOnCompleteListener(new OnCompleteListener<AuthResult>() {
            @Override
            public void onComplete(@NonNull Task<AuthResult> task) {
                if(task.isSuccessful()){

                    startActivity(new Intent(Login.this, NewHome.class));//////////////////////////
                }
                else{
                    Toast.makeText(Login.this, "We could not start your session. Check yur data", Toast.LENGTH_SHORT).show();
                }
            }
        });


    }
}