package com.example.hobbe;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.lang.reflect.Array;
import java.util.ArrayList;

public class SQLLiteManager  extends SQLiteOpenHelper {

    public SQLLiteManager(Context context){
        super(context, "android_cache",null,1);


    }
    @Override
    public void onCreate(SQLiteDatabase db) {

        String sql = "CREATE TABLE IF NOT EXISTS activities_v2( name TEXT NOT NULL, temperature TEXT NOT NULL, id INTEGER NOT NULL, description TEXT, location TEXT, address TEXT, date TEXT, hour TEXT, image INTEGER, category TEXT, cost TEXT, organizer TEXT, grupaln TEXT, lat TEXT, lon TEXT)";
        String query2 = "CREATE TABLE IF NOT EXISTS activities_trending_v2( name TEXT NOT NULL, temperature TEXT NOT NULL, id INTEGER NOT NULL, description TEXT, location TEXT, address TEXT, date TEXT, hour TEXT, image INTEGER, category TEXT, cost TEXT, organizer TEXT, grupaln TEXT, lat TEXT, lon TEXT)";

        db.execSQL(sql);
        db.execSQL(query2);








    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

        db.execSQL( " DROP TABLE activities_v2 ");
        db.execSQL( " DROP TABLE activities_trending_v2 ");
        onCreate(db);

    }

    public void addData(AuxiliarEvent aux_event){



        SQLiteDatabase  sqLiteDAtabase =  this.getWritableDatabase();
        ContentValues   contentValues = new ContentValues();

        contentValues.put("name", aux_event.getName());
        contentValues.put("temperature", aux_event.getTemperature());
        contentValues.put("id", aux_event.getId());
        contentValues.put("description", aux_event.getDescription());
        contentValues.put("location", aux_event.getLocation());
        contentValues.put("address", aux_event.getAddress());
        contentValues.put("date", aux_event.getDate());
        contentValues.put("hour", aux_event.getHour());
        contentValues.put("image", aux_event.getImage());
        contentValues.put("category", aux_event.getCategory());
        contentValues.put("cost", aux_event.getCost());
        contentValues.put("organizer", aux_event.getOrganizer());
        contentValues.put("grupaln", aux_event.getGrupaln());
        contentValues.put("lat", aux_event.getLat());
        contentValues.put("lon", aux_event.getLon());

        sqLiteDAtabase.insert("activities_v2", null, contentValues);
        sqLiteDAtabase.close();
    }

    public void addDataTrending(AuxiliarEvent aux_event){
        SQLiteDatabase  sqLiteDAtabase =  this.getWritableDatabase();
        ContentValues   contentValues = new ContentValues();

        contentValues.put("name", aux_event.getName());
        contentValues.put("temperature", aux_event.getTemperature());
        contentValues.put("id", aux_event.getId());
        contentValues.put("description", aux_event.getDescription());
        contentValues.put("location", aux_event.getLocation());
        contentValues.put("address", aux_event.getAddress());
        contentValues.put("date", aux_event.getDate());
        contentValues.put("hour", aux_event.getHour());
        contentValues.put("image", aux_event.getImage());
        contentValues.put("category", aux_event.getCategory());
        contentValues.put("cost", aux_event.getCost());
        contentValues.put("organizer", aux_event.getOrganizer());
        contentValues.put("grupaln", aux_event.getGrupaln());
        contentValues.put("lat", aux_event.getLat());
        contentValues.put("lon", aux_event.getLon());




        sqLiteDAtabase.insert("activities_trending_v2", null, contentValues);
        sqLiteDAtabase.close();

    }


    public ArrayList<AuxiliarEvent> getData(){

        ArrayList<AuxiliarEvent> lista = new ArrayList<AuxiliarEvent>();
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor  = db.rawQuery("SELECT * FROM activities_v2", null);
        if (cursor.moveToFirst()){
            do{
                 AuxiliarEvent aux = new AuxiliarEvent(cursor.getString(0), cursor.getString(1),cursor.getInt(2),
                         cursor.getString(3),cursor.getString(4),cursor.getString(5),cursor.getString(6),
                         cursor.getString(7),cursor.getInt(8),cursor.getString(9),cursor.getString(11),
                         cursor.getString(11),cursor.getString(12),cursor.getString(13),cursor.getString(14)

                         );
                 lista.add(aux);
            }while (cursor.moveToNext());


        }

        return lista;

    }


    public ArrayList<AuxiliarEvent> getDataTrending(){

        ArrayList<AuxiliarEvent> lista = new ArrayList<AuxiliarEvent>();
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor  = db.rawQuery("SELECT * FROM activities_trending_v2", null);
        if (cursor.moveToFirst()){
            do{
                AuxiliarEvent aux = new AuxiliarEvent(cursor.getString(0), cursor.getString(1),cursor.getInt(2),
                        cursor.getString(3),cursor.getString(4),cursor.getString(5),cursor.getString(6),
                        cursor.getString(7),cursor.getInt(8),cursor.getString(9),cursor.getString(11),
                        cursor.getString(11),cursor.getString(12),cursor.getString(13),cursor.getString(14)

                );
                lista.add(aux);
            }while (cursor.moveToNext());


        }

        return lista;

    }

    public  void clenaTable(){
        SQLiteDatabase  db =  this.getWritableDatabase();
        db.execSQL( " DELETE FROM activities_v2 ");

    }
    public  void clenaTableTrending(){
        SQLiteDatabase  db =  this.getWritableDatabase();
        db.execSQL( " DELETE FROM activities_trending_v2 ");

    }





}
