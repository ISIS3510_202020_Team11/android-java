package com.example.hobbe;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.widget.ArrayAdapter;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.lorentzos.flingswipe.SwipeFlingAdapterView;

import java.util.ArrayList;

public class Tinder1 extends AppCompatActivity {

    private ArrayList<String> al;
    private ArrayAdapter<String> arrayAdapter;
    private int i;
    SwipeFlingAdapterView flingContainer;
    private FirebaseAuth mAuth;
    private DatabaseReference  usersDB;
    int cuenta = 0;
    ArrayList<AuxiliarEvent> activities;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        //User data
        mAuth = FirebaseAuth.getInstance();
        final String currentUserId = mAuth.getCurrentUser().getUid();
        usersDB = FirebaseDatabase.getInstance().getReference().child("Users");

        setContentView(R.layout.tinder1);
         al = new ArrayList<>();
        activities = new ArrayList() ;
        flingContainer = findViewById(R.id.frame);

        SharedPreferences pre = getSharedPreferences("tinder", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor =  pre.edit();
        //



        // Trar actividades para swipe
        // if ( pre.getInt("flag", 0) == 0 ){
             String v = "Dark Gray" +  "\n" + "\n"+"Inz";
            al.add( v);
            bringData();
           // editor.putInt("flag", 1);
         //   editor.commit();
        //}

        arrayAdapter = new ArrayAdapter(Tinder1.this, R.layout.item, R.id.helloText, al );
        flingContainer.setAdapter(arrayAdapter);
        flingContainer.setFlingListener(new SwipeFlingAdapterView.onFlingListener() {
            @Override
            public void removeFirstObjectInAdapter() {
                // this is the simplest way to delete an object from the Adapter (/AdapterView)
                Log.d("LIST", "removed object!");
                al.remove(0);
                arrayAdapter.notifyDataSetChanged();
            }

            @Override
            public void onLeftCardExit(Object dataObject) {
                //Do something on the left!
                //You also have access to the original object.
                //If you want to use it just cast it (String) dataObject
                usersDB.child(currentUserId).child("HobbiesNot").child(String.valueOf(cuenta)).setValue((String) dataObject);
                cuenta++;
                al.remove((String) dataObject);
                Toast.makeText(Tinder1.this, "NO!", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onRightCardExit(Object dataObject) {
                al.remove((String) dataObject);
                usersDB.child(currentUserId).child("Hobbies").child(String.valueOf(cuenta)).setValue((String) dataObject);
                cuenta++;
                Toast.makeText(Tinder1.this, "YES!", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onAdapterAboutToEmpty(int itemsInAdapter) {
                // Ask for more data here
                //al.add("XML ".concat(String.valueOf(i)));
                //arrayAdapter.notifyDataSetChanged();
                //Log.d("LIST", "notified");
                //i++;
            }

            @Override
            public void onScroll(float scrollProgressPercent) {
            }
        });


        // Optionally add an OnItemClickListener
        flingContainer.setOnItemClickListener(new SwipeFlingAdapterView.OnItemClickListener() {
            @Override
            public void onItemClicked(int itemPosition, Object dataObject) {
                //Toast.makeText(Tinder1.this, "Click!", Toast.LENGTH_SHORT).show();
                Intent intent = new Intent(Tinder1.this,DetailActActivity.class);
                intent.putExtra("name" ,activities.get(itemPosition).getName() );
                intent.putExtra("id" ,activities.get(itemPosition).getId() );
                intent.putExtra("location" ,activities.get(itemPosition).getLocation() );
                intent.putExtra("address" ,activities.get(itemPosition).getAddress() );
                intent.putExtra("date" ,activities.get(itemPosition).getDate() );
                intent.putExtra("hour" ,activities.get(itemPosition).getHour() );
                intent.putExtra("image" ,activities.get(itemPosition).getImage() );
                intent.putExtra("category" ,activities.get(itemPosition).getCategory() );
                intent.putExtra("cost" ,activities.get(itemPosition).getCost() );
                intent.putExtra("lat" ,activities.get(itemPosition).getLon() );
                intent.putExtra("long" ,activities.get(itemPosition).getLat() );
                Tinder1.this.startActivity(intent);


            }
        });

    }

    public void bringData(){

        DatabaseReference ref = FirebaseDatabase.getInstance().getReference().child("Tinder");
        ref.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                String name, temperature,description, location, address, date, hour,category, cost,organizer;
                int grupaln;
                name = "";
                temperature="";
                description="";
                location="";
                address="";
                date = "";
                hour="";
                category="";
                cost = "";
                organizer = "";

                int id = 0;
                double lat = 0;
                double lon = 0;

                AuxiliarEvent aux = null;
                for (DataSnapshot snapshot: dataSnapshot.getChildren()) {

                     id = Integer.valueOf(snapshot.child("id").getValue().toString());
                     name = snapshot.child("Name").getValue().toString();
                    //int temperature = Integer.valueOf(snapshot.child("Weather").getValue().toString());
                     description = snapshot.child("Description").getValue().toString();
                     location = snapshot.child("Location").getValue().toString();
                     address = snapshot.child("Address").getValue().toString();
                     date = snapshot.child("Date").getValue().toString();
                     hour = snapshot.child("Hour").getValue().toString();

                     category = snapshot.child("Category").getValue().toString();
                     cost = snapshot.child("Cost").getValue().toString();
                     organizer = snapshot.child("Organizer").getValue().toString();
                     grupaln = Integer.valueOf(snapshot.child("Grupal").getValue().toString());
                     lat = Double.valueOf(snapshot.child("lat").getValue().toString().trim());
                     lon = Double.valueOf(snapshot.child("long").getValue().toString().trim());

                    int img = id > 5? R.drawable.biking: R.drawable.swimming;

                    aux = new AuxiliarEvent(category,"23",id,description,location,address
                            ,date,hour,img,category,cost,organizer,String.valueOf(grupaln),String.valueOf(lat),String.valueOf(lon));

                    if (!al.contains(category) ) al.add(category  + "\n" + "\n" + description );
                    activities.add(aux);

                }


            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        });
    }




}