package com.example.hobbe;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.RecyclerView;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.List;


public class Adapter_V2 extends RecyclerView.Adapter<Adapter_V2.ViewHolder> {

    private FirebaseAuth mAuth;
    private DatabaseReference usersDB;
    final String currentUserId;

    //List<String> titles;
    //List<Integer> images;
    LayoutInflater inflater;
    List<AuxiliarEvent> activities;
    Context context;


    public Adapter_V2(Context ctx, List<AuxiliarEvent> acts){
        this.activities = acts;
        this.inflater =  LayoutInflater.from(ctx);
        this.context = ctx;

        mAuth = FirebaseAuth.getInstance();
        currentUserId = mAuth.getCurrentUser().getUid();
        usersDB = FirebaseDatabase.getInstance().getReference().child("Users");


    }



    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = inflater.inflate(R.layout.custom_grid_layout,parent,false);

        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.title.setText(activities.get(position).getName());
        int img = activities.get(position).getId() > 15? R.drawable.biking:activities.get(position).getId()  >8? R.drawable.swimming: R.drawable.hiking;
        holder.image.setImageResource(img);

        final  AuxiliarEvent aux = activities.get(position);
        holder.clickable_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context,DetailActActivity.class);
                intent.putExtra("name" ,aux.getName() );
                intent.putExtra("id" ,aux.getId() );
                intent.putExtra("location" ,aux.getLocation() );
                intent.putExtra("address" ,aux.getAddress() );
                intent.putExtra("date" ,aux.getDate() );
                intent.putExtra("hour" ,aux.getHour() );
                intent.putExtra("image" ,aux.getImage() );
                intent.putExtra("category" ,aux.getCategory() );
                intent.putExtra("cost" ,aux.getCost() );
                intent.putExtra("lat" ,aux.getLon() );
                intent.putExtra("long" ,aux.getLat() );
                context.startActivity(intent);
                //Toast.makeText(v.getContext(),"Hola",Toast.LENGTH_SHORT).show();

                usersDB.child(currentUserId).child("Interactions").child(String.valueOf(System.currentTimeMillis())).setValue(aux.getId());

            }
        });

    }

    @Override
    public int getItemCount() {
        return activities.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{

        TextView title;
        ImageView image;
        ConstraintLayout clickable_layout;


        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            title =itemView.findViewById(R.id.text_title);
            image = itemView.findViewById(R.id.Image);
            clickable_layout = itemView.findViewById(R.id.layout);


        }
    }
}
