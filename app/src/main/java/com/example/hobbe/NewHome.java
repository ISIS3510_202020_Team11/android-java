package com.example.hobbe;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Context;
import android.content.Intent;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class NewHome extends AppCompatActivity implements SensorEventListener {

    private Button mButtonSingOut;
    private FirebaseAuth mAuth;

    private TextView textViewResult;
    private RequestQueue mQueue;

    private SensorManager sensorMan;
    private Sensor tempSensor;
    private float temp = 23;
    private boolean isTempAvai = false;
    private SQLLiteManager sqlLiteManager;


   // public static int flag = 0;

    RecyclerView trending;
    Adapter_V2 trendingAdapter;
    List<String> titlesTrending;
    List<Integer> imagesTrending;



    RecyclerView dataList;
    List<String> titles;
    List<Integer> images;

    Adapter_V2 adapter;

    ImageView profile;

    ImageView tinder;


    Button  buttonRecommendations;


    trendingThread th  = new trendingThread();
    ///////////Instance for messagess
    static double tempV2= 23;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_home);


        //Queue de volley
        mQueue = Volley.newRequestQueue(NewHome.this);
        // Go to profile
        profile = findViewById(R.id.profile);
        profile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                 startActivity(new Intent(NewHome.this, Userprofile_act.class));
                //throw new RuntimeException("Test Crash");
            }
        });

        //Navigating to tinder part

        tinder = findViewById(R.id.tinder);
        tinder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (ConnectivityCheck()) {startActivity(new Intent(NewHome.this, Tinder1.class));}
                else{Toast.makeText(NewHome.this, "There is not Internet connection.",Toast.LENGTH_SHORT).show();}
            }
        });
        // Log out

        mAuth = FirebaseAuth.getInstance();


        mButtonSingOut = (Button) findViewById(R.id.btnSingout);
        mButtonSingOut.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mAuth.signOut();
                startActivity(new Intent(NewHome.this, MainActivity.class));
                finish();

            }
        });

        //Sensores
        // Sensores
        // Sensores
        textViewResult = findViewById(R.id.recom);
        sensorMan = (SensorManager) getSystemService(Context.SENSOR_SERVICE);
        if (sensorMan.getDefaultSensor(Sensor.TYPE_AMBIENT_TEMPERATURE)!= null){
            tempSensor = sensorMan.getDefaultSensor(Sensor.TYPE_AMBIENT_TEMPERATURE);
            isTempAvai = true;
        }

         //--------------------------------Solve temperature issue
         //getTemperatureFromAPI();
        //textViewResult.append(String.valueOf(tempV2));







        //Recycler View ---------------------------------------------------------------------------
        dataList = findViewById(R.id.dataList);

        titles = new ArrayList<String>();
        images = new ArrayList<Integer>();

        //Cache --------------------------------------------------------------------------------
        sqlLiteManager = new SQLLiteManager(this);


        if (!sqlLiteManager.getData().isEmpty()){
            ArrayList <AuxiliarEvent> cache = sqlLiteManager.getData();

            adapter = new Adapter_V2(NewHome.this, cache);
            GridLayoutManager gridLayoutManager_cache = new GridLayoutManager(this, 2,GridLayoutManager.HORIZONTAL,false);
            dataList.setLayoutManager(gridLayoutManager_cache);
            dataList.setAdapter(adapter);

        }

        //Sistema de recomendacion
        buttonRecommendations =  findViewById(R.id.btnRecommendations);


        buttonRecommendations.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (ConnectivityCheck()){

                    //RecommendationsTask g = new RecommendationsTask();
                    //RecommendationsThread rthread = new RecommendationsThread();
                    //rthread.start();
                     recommendation thread = new recommendation();
                     thread.execute();

                    //GridLayoutManager gridLayoutManager = new GridLayoutManager(NewHome.this, 2,GridLayoutManager.HORIZONTAL,false);
                    //dataList.setLayoutManager(gridLayoutManager);
                    //rthread.interrupt();
                }

            }
        });

        // We create here the layout of the app


        // Trending activity
        trending = findViewById(R.id.Trending);
        titlesTrending = new ArrayList<String>();
        imagesTrending = new ArrayList<Integer>();

        if (ConnectivityCheck()) {


            th.start();

        }
        else {

            ArrayList data_trending = sqlLiteManager.getDataTrending();


            trendingAdapter = new Adapter_V2(NewHome.this,data_trending);
            trending.setAdapter(trendingAdapter);

        }

        GridLayoutManager gridLayoutManagerTrending = new GridLayoutManager(this, 1,GridLayoutManager.HORIZONTAL,false);
        trending.setLayoutManager(gridLayoutManagerTrending);

    }



    /**
     * Asks for temperatura ale to the sensor
     * @param event
     */
    @Override
    public void onSensorChanged(SensorEvent event) {
        temp = event.values[0];

    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {

    }

    @Override
    protected void onResume() {
        super.onResume();
        if(isTempAvai){
            sensorMan.registerListener(this, tempSensor, SensorManager.SENSOR_DELAY_NORMAL);
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        if(isTempAvai){
            sensorMan.unregisterListener(this);
        }

    }


    private  void JsonParse(){

        Random random = new Random();

        int randomNumber = random.nextInt(29) + 1;
        String id = String.valueOf(randomNumber);
        String url =  "https://willmn.pythonanywhere.com/api/recommendations?id="+id;
        //final ArrayList<AuxiliarEvent> activities2 = new ArrayList();
        JsonArrayRequest request = new JsonArrayRequest(Request.Method.GET, url, null, new Response.Listener<JSONArray>() {
            @Override
            public void onResponse(JSONArray response) {

                titles.clear();
                images.clear();
                sqlLiteManager.clenaTable();

                ArrayList<AuxiliarEvent> activities = new ArrayList<AuxiliarEvent>();

                try {
                    int id = 0;
                    String name = "";
                    int temperature = 0;
                    String description ="";
                    String location = "";
                    String address = "";
                    String date = "";
                    String hour = "";
                    int image = 0;
                    String category = "";
                    String cost ="";
                    String organizer = "";
                    int  grupaln =0;
                    double lat = 0;
                    double lon = 0;
                    int img = 0;
                    AuxiliarEvent aux = null;

                    for(int i =0; i<response.length();i++) {
                        JSONObject object = response.getJSONObject(i);

                        //Pkey value pairs from  json file
                         id = object.getInt("id");
                         name = object.getString("Name");
                         temperature = object.getInt("Weather ");
                         description = object.getString("Description");
                         location = object.getString("Location");
                         address = object.getString("Address");
                         date = object.getString("Date");
                         hour = object.getString("Hour");
                         image = object.getInt("Image");
                         category = object.getString("Category");
                         cost = object.getString("Cost");
                         organizer = object.getString("Organizer");
                          grupaln = object.getInt("Grupal");
                          lat = object.getInt("lat");
                          lon = object.getInt("long");


                         img = id > 16? R.drawable.biking: id >5? R.drawable.swimming: R.drawable.hiking;

                        if (temperature <= tempV2 +7 && temperature >= tempV2 -7){

                             aux = new AuxiliarEvent(name,String.valueOf(temperature),id,description,location,address
                            ,date,hour,img,category,cost,organizer,String.valueOf(grupaln),String.valueOf(lat),String.valueOf(lon));

                            activities.add(aux);
                            sqlLiteManager.addData(aux);

                        }
                    }

                    //Le agrega al grid layout las recomendaciones
                    //activities2 = activities;
                    adapter = new Adapter_V2(NewHome.this,activities);
                    dataList.setAdapter(adapter);

                } catch (JSONException e) {
                    e.printStackTrace();
                }


            }

        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        });
        mQueue.add(request);


    }

    /**
     * Checks if the user has inernet
     * @return
     */
    public  boolean ConnectivityCheck(){
        ConnectivityManager manager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = manager.getActiveNetworkInfo();
        if (networkInfo != null  && networkInfo.isConnected()){

            return true;
        }
        else{

            AlertMessage alert = new AlertMessage();
            alert.show(getSupportFragmentManager(),"connection dialog");
            //Toast.makeText(NewHome.this, "There is not Internet connection. You will only see the most recent recommendations",Toast.LENGTH_LONG).show();
            return false;
        }

    }

    public  void  retrieveTrendingActivity(){

        //final ArrayList<String> titles1 = new ArrayList<String>();
        //final  ArrayList<Integer> images1 =  new ArrayList<Integer>();

        DatabaseReference ref = FirebaseDatabase.getInstance().getReference().child("TrendingActivities");
        sqlLiteManager.clenaTableTrending();

        ref.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                ArrayList<AuxiliarEvent> aux_list = new ArrayList<AuxiliarEvent>();

                int id = 0;
                String name = "";
                //int temperature = Integer.valueOf(snapshot.child("Weather").getValue().toString());
                String description ="";
                String location = "";
                String address = "";
                String date = "";
                String hour = "";
                int image = 0;
                String category = "";
                String cost ="";
                String organizer = "";
                int  grupaln =0;
                double lat = 0;
                double lon = 0;
                int img = 0;
                AuxiliarEvent aux = null;
                for (DataSnapshot snapshot: dataSnapshot.getChildren()) {

                     id = Integer.valueOf(snapshot.child("id").getValue().toString());
                     name = snapshot.child("Name").getValue().toString();
                    //int temperature = Integer.valueOf(snapshot.child("Weather").getValue().toString());
                     description = snapshot.child("Description").getValue().toString();
                     location = snapshot.child("Location").getValue().toString();
                     address = snapshot.child("Address").getValue().toString();
                     date = snapshot.child("Date").getValue().toString();
                     hour = snapshot.child("Hour").getValue().toString();
                     image = Integer.valueOf(snapshot.child("Image").getValue().toString());
                     category = snapshot.child("Category").getValue().toString();
                     cost = snapshot.child("Cost").getValue().toString();
                     organizer = snapshot.child("Organizer").getValue().toString();
                      grupaln = Integer.valueOf(snapshot.child("Grupal").getValue().toString());
                     lat = Double.valueOf(snapshot.child("lat").getValue().toString().trim());
                     lon = Double.valueOf(snapshot.child("long").getValue().toString().trim());

                     img = id > 10? R.drawable.loginimage:id >14? R.drawable.swimming: R.drawable.hiking;

                     aux = new AuxiliarEvent(name,"23",id,description,location,address
                            ,date,hour,img,category,cost,organizer,String.valueOf(grupaln),String.valueOf(lat),String.valueOf(lon));

                    aux_list.add(aux);


                    //titlesTrending.add(snapshot.child("name").getValue().toString());
                    //imagesTrending.add(R.drawable.parachutehome);  // Se ejecuta de forma asincrona

                    // Lo guarda en cache
                    sqlLiteManager.addDataTrending(aux);

                }
                // Termina y lo pone en el gridview
                trendingAdapter = new Adapter_V2(NewHome.this,aux_list);
                trending.setAdapter(trendingAdapter);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
            }
        });


    }


    ///////////////////////Thread Management
    class trendingThread extends Thread {

        @Override
        public void run() {
            retrieveTrendingActivity();
        }
    }
    class RecommendationsThread extends  Thread{
        @Override
        public void run() {
            JsonParse();
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        th.interrupt();
    }


    public void  getTemperatureFromAPI(){

        String url = "https://api.openweathermap.org/data/2.5/weather?q=Bogota,co&APPID=80b9e78d2ece93ea5a778e8d55c2f5dc" ;
        JsonObjectRequest jor = new JsonObjectRequest(Request.Method.GET, url, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                try {
                    JSONObject  go = response.getJSONObject("main");
                    tempV2  = go.getDouble("temp") - 273.15;

                } catch (JSONException e) {
                    e.printStackTrace();
                }


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                //textViewResult.append(error.getMessage());
                textViewResult.append("hola");
                tempV2 = 23;

            }
        });
        RequestQueue g = Volley.newRequestQueue(NewHome.this);
        g.add(jor);
    }

    ArrayList asyn_acts = new ArrayList();


    //Threads with asynTasks
    private class recommendation extends AsyncTask<Void, Void, ArrayList<AuxiliarEvent> >{

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            buttonRecommendations.setEnabled(false);

        }

        @Override
        protected ArrayList<AuxiliarEvent> doInBackground(Void... voids) {
           JsonParse2();
            return asyn_acts;
        }

        @Override
        protected void onPostExecute(ArrayList<AuxiliarEvent> auxiliarEvents) {
            //super.onPostExecute(auxiliarEvents);

            GridLayoutManager gridLayoutManager = new GridLayoutManager(NewHome.this, 2,GridLayoutManager.HORIZONTAL,false);
            dataList.setLayoutManager(gridLayoutManager);



            buttonRecommendations.setEnabled(true);
        }


    }


    private  void JsonParse2(){

        Random random = new Random();

        int randomNumber = random.nextInt(29) + 1;
        String id = String.valueOf(randomNumber);
        String url =  "https://willmn.pythonanywhere.com/api/recommendations?id="+id;
        //final ArrayList<AuxiliarEvent> activities2 = new ArrayList();
        JsonArrayRequest request = new JsonArrayRequest(Request.Method.GET, url, null, new Response.Listener<JSONArray>() {
            @Override
            public void onResponse(JSONArray response) {

                asyn_acts.clear();
                //titles.clear();
                //images.clear();
                sqlLiteManager.clenaTable();

                //ArrayList<AuxiliarEvent> activities = new ArrayList<AuxiliarEvent>();

                try {
                    int id = 0;
                    String name = "";
                    int temperature = 0;
                    String description ="";
                    String location = "";
                    String address = "";
                    String date = "";
                    String hour = "";
                    int image = 0;
                    String category = "";
                    String cost ="";
                    String organizer = "";
                    int  grupaln =0;
                    double lat = 0;
                    double lon = 0;
                    int img = 0;
                    AuxiliarEvent aux = null;

                    for(int i =0; i<response.length();i++) {
                        JSONObject object = response.getJSONObject(i);

                        //Pkey value pairs from  json file
                        id = object.getInt("id");
                        name = object.getString("Name");
                        temperature = object.getInt("Weather ");
                        description = object.getString("Description");
                        location = object.getString("Location");
                        address = object.getString("Address");
                        date = object.getString("Date");
                        hour = object.getString("Hour");
                        image = object.getInt("Image");
                        category = object.getString("Category");
                        cost = object.getString("Cost");
                        organizer = object.getString("Organizer");
                        grupaln = object.getInt("Grupal");
                        lat = object.getInt("lat");
                        lon = object.getInt("long");


                        img = id > 16? R.drawable.biking: id >5? R.drawable.swimming: R.drawable.hiking;

                        if (temperature <= tempV2 +7 && temperature >= tempV2 -7){

                            aux = new AuxiliarEvent(name,String.valueOf(temperature),id,description,location,address
                                    ,date,hour,img,category,cost,organizer,String.valueOf(grupaln),String.valueOf(lat),String.valueOf(lon));

                            asyn_acts.add(aux);
                            sqlLiteManager.addData(aux);

                        }
                    }


                    adapter = new Adapter_V2(NewHome.this,asyn_acts);
                    dataList.setAdapter(adapter);

                } catch (JSONException e) {
                    e.printStackTrace();
                }


            }

        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        });
        mQueue.add(request);
    }



}